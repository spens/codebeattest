//
//  TestClass.swift
//  cbtest
//
//  Created by spens on 15/06/2018.
//  Copyright © 2018 cbtest. All rights reserved.
//

import Foundation

class SomeClass {
    
    func someFnc() {
        let a = t2 + t3
    }
    
    var someVr: String {
        return "sdfsdfsdf"
    }
    
    // codebeat:disable[ABC,ARITY,BLOCK_NESTING,CYCLO,TOTAL_COMPLEXITY,TOO_MANY_FUNCTIONS,TOO_MANY_IVARS,LOC,TOTAL_LOC]
    var t1: String {
        return ""
    }
    var t2: String {
        return ""
    }
    var t3: String {
        return ""
    }
    var t4: String {
        return ""
    }
    var t5: String {
        return ""
    }
    var t6: String {
        return ""
    }
    var t7: String {
        return ""
    }
    var t8: String {
        return ""
    }
    var t9: String {
        return ""
    }
    var t10: String {
        return ""
    }
    var t11: String {
        return ""
    }
    var t12: String {
        return ""
    }
    var t13: String {
        return ""
    }
    var t14: String {
        return ""
    }
    var t15: String {
        return ""
    }
    var t16: String {
        return ""
    }
    var t17: String {
        return ""
    }
    var t18: String {
        return ""
    }
    var t19: String {
        return ""
    }
    var t20: String {
        return ""
    }
    // codebeat:enable[ABC,ARITY,BLOCK_NESTING,CYCLO,TOTAL_COMPLEXITY,TOO_MANY_FUNCTIONS,TOO_MANY_IVARS,LOC,TOTAL_LOC]
}
